# vim-fle-syntax

VIM syntax plugin for writing Fast Log Entry (https://df3cb.com/fle/) compatible log files in VIM

# Screenshot

![screenshot](/screenshot.jpg?raw=true "screenshot")

## Description

Vim syntax file to imitate the syntax highting of Fast Log Entry (FLE).

## Why?

Because it's nice to take a small GNU/Linux based laptop with me on portable operations, just log everything with VIM and do the rest at home.

## Install

Copy or symlink these files to your $VIMRUNTIME/syntax and $VIMRUNTIME/ftdetect directory or to
~/.vim/syntax and ~/.vim/ftdetect directories
